# TrupikAuthLib

[![CI Status](https://img.shields.io/travis/praveenkumarkonathala@gmail.com/TrupikAuthLib.svg?style=flat)](https://travis-ci.org/praveenkumarkonathala@gmail.com/TrupikAuthLib)
[![Version](https://img.shields.io/cocoapods/v/TrupikAuthLib.svg?style=flat)](https://cocoapods.org/pods/TrupikAuthLib)
[![License](https://img.shields.io/cocoapods/l/TrupikAuthLib.svg?style=flat)](https://cocoapods.org/pods/TrupikAuthLib)
[![Platform](https://img.shields.io/cocoapods/p/TrupikAuthLib.svg?style=flat)](https://cocoapods.org/pods/TrupikAuthLib)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements
Need to Requirements

## Installation

TrupikAuthLib is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TrupikAuthLib'
```

## Author

PK, praveenkumarkonathala@gmail.com

## License

TrupikAuthLib is available under the MIT license. See the LICENSE file for more info.

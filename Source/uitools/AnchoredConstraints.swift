//
//  AnchoredConstraints.swift
//  LBTATools
//
//  Created by Praveen kumar konathala on 5/1/19.
//

import UIKit

public struct AnchoredConstraints {
    public var top, leading, bottom, trailing, width, height: NSLayoutConstraint?
}

//
//  View+nav+extension.swift
//  TrupikFy
//
//  Created by admin on 03/06/19.
//  Copyright © 2019 trupik. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func loadVC(vc: UIViewController){
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func removeVC(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
}

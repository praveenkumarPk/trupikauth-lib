//
//  ViewController.swift
//  TrupikAuthLib
//
//  Created by praveenkumarkonathala@gmail.com on 06/24/2019.
//  Copyright (c) 2019 praveenkumarkonathala@gmail.com. All rights reserved.
//

import UIKit
import TrupikAuthLib

class ViewController: UIViewController {
    @IBOutlet weak var image_outlet: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        image_outlet.circleImageView(borderColor: .red, borderWidth: 2.0)
         
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


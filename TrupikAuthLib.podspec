#
# Be sure to run `pod lib lint TrupikAuthLib.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TrupikAuthLib'
  s.version          = '0.1.7'
  s.summary          = 'Firebase Auth for user with Trupik Auth'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'This is For FirebaseAuth Setup, With Social Login Integration and will be provide more information Soon'

  s.homepage         = 'https://bitbucket.org/praveenkumarPk/trupikauth-lib/'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Pk' => 'praveenkumarkonathala@gmail.com' }
  s.source           = { :git => 'https://praveenkumarPk@bitbucket.org/praveenkumarPk/trupikauth-lib.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '12.2'

  s.source_files = 'Source/*.swift'
  #s.frameworks = 'UIKit'
  s.swift_version = '5.0'
  s.platforms = {
    "ios": "12.2"
  }
  # s.resource_bundles = {
  #   'TrupikAuthLib' => ['TrupikAuthLib/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.static_framework = true
  s.dependency 'SVProgressHUD'
  s.dependency 'IQKeyboardManager'
  s.dependency 'GoogleSignIn'
  s.dependency 'Firebase/Auth'
  s.dependency 'Firebase/Analytics'
  s.dependency 'Firebase/Core'
end
